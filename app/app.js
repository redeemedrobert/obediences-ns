import Vue from "nativescript-vue";
import App from "./components/App";
import Today from "./components/Today";
import DrawerContent from "./components/DrawerContent";
import RadSideDrawer from "nativescript-ui-sidedrawer/vue";
import * as fs from './shared/fs'

require('nativescript-local-notifications')

Vue.use(RadSideDrawer);

Vue.registerElement(
  'CheckBox',
  () => require('@nstudio/nativescript-checkbox').CheckBox,
  {
    model: {
      prop: 'checked',
      event: 'checkedChange'
    }
  }
);

Vue.config.silent = (TNS_ENV === 'production');

export const appState = Vue.observable({
  dailyObds: [/*
    {
      key: new Date().getTime(),
      title: 'Morning Prayers',
      completedDate: 'incomplete',
      completedAt: '8:45',
      notifyAt: '8:00'
    },
    {
      key: new Date().getTime()+1,
      title: 'Canon of Repentance',
      completedDate: 'incomplete',
      completedAt: '13:00',
      notifyAt: '13:00'
    },
    {
      key: new Date().getTime()+2,
      title: 'Evening Prayers',
      completedDate: 'incomplete',
      completedAt: 'incomplete',
      notifyAt: '21:00'
    },*/
  ],

  scheduledObds:[/*
    {
      key: new Date().getTime()+4,
      title: "Back to the office",
      date: '20200608',
      time: '9:00',
      completed: false
    },
    {
      key: new Date().getTime()+5,
      title: "Men's Group",
      date: '20200615',
      time: '19:00',
      completed: false
    },
    {
      key: new Date().getTime()+6,
      title: "Canon of Augustine",
      date: '20200615',
      time: '22:00',
      completed: false
    },
    {
      key: new Date().getTime()+7,
      title: "Vacation starts",
      date: '20200706',
      time: '19:00',
      completed: false
    },
    {
      key: new Date().getTime()+8,
      title: "Next year",
      date: '20210101',
      time: '10:00',
      completed: false
    },*/
  ],

  showToast: false,
  toastMessage: 'Saved successfully.'
})

new Vue({

    /*data:{
      dailyObds: [
        {
          key: new Date().getTime(),
          title: "Morning Prayers",
          completed: true,
          completedAt: "8:45",
          notifyAt: "8:00"
        },
        {
          key: new Date().getTime()+1,
          title: "Canon of Repentance",
          completed: true,
          completedAt: "13:00",
          notifyAt: "13:00"
        },
        {
          key: new Date().getTime()+2,
          title: "Evening Prayers",
          completed: false,
          completedAt: "incomplete",
          notifyAt: "21:00"
        },
      ],

      showToast: 'hidden',
      toastMessage: 'Saved successfully.'
    },*/

    /*methods:{
      toggleDailyObd(index){
        const newObds = this.dailyObds.map((obd, i)=>{
          if(i===index){
              const newObd = obd
              newObd.completed = !obd.completed
              return newObd
          } else return obd
        })
        this.dailyObds = newObds
      }
    },*/

    /*watch:{
      dailyObds: function(){
        this.showToast = 'visible'
      }
    },*/

    created: function(){
      fs.loadObdsFromFile()
    },

    render (h) {
      return h(
          App,//root component
          //{//props
          // props:{
          //   showToast: this.showToast,
          //    toastMessage: this.toastMessage
          //  }
          //},
          [//child components
            h(DrawerContent, { 
              slot: 'drawerContent',
              //props:{
              //  dailyObds: this.dailyObds,
              //  showToast: this.showToast
              //}
            }),
            h(Today, {
              slot: 'mainContent',
              //props:{
              //  dailyObds: this.dailyObds,
              //  showToast: this.showToast,
              //},
            })
          ]
        )
      }
    }).$start();
