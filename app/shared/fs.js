import { File, knownFolders } from 'tns-core-modules/file-system'
import { appState } from '../app'

const docs = knownFolders.documents()
const dobds = docs.getFile('dailyobds.json')
const sobds = docs.getFile('schedobds.json')

export function loadObdsFromFile(){
    dobds.readText().then(
        (res)=>{
            try{
                appState.dailyObds = JSON.parse(res)
            }catch(err){
                console.error('Unable to parse json from dailyobds.json:', err)
            }
        },
        (err)=>{
            console.error("Unable to read dailyobds.json:", err)
        }
    )
    sobds.readText().then(
        (res)=>{
            try{
                appState.scheduledObds = JSON.parse(res)
            }catch(err){
                console.error('Unable to parse json from schedobds.json:', err)
            }
        },
        (err)=>{
            console.error("Unable to read schedobds.json:", err)
        }
    )
}

export function saveDailyObds(){
    dobds.writeText(JSON.stringify(appState.dailyObds)).then(
        ()=>{
            console.log('Successfully saved daily obediences to dailyobds.json')
            appState.toastMessage = 'Change(s) saved successfully!'
            appState.showToast = true
            setTimeout(()=>appState.showToast=false, 3000)
        },
        (err)=>console.error('Unable to save daily obediences to dailyobds.json:', err)
    )
}

export function saveSchedObds(){
    sobds.writeText(JSON.stringify(appState.scheduledObds)).then(
        ()=>{
            console.log('Successfully saved scheduled obediences to schedobds.json')
            appState.toastMessage = 'Change(s) saved successfully!'
            appState.showToast = true
            setTimeout(()=>appState.showToast=false, 3000)
        },
        (err)=>console.error('Unable to save scheduled obediences to schedobds.json:', err)
    )
}