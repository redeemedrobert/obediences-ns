
import { getRootView } from "tns-core-modules/application"

export const showDrawer = () => {
    let drawerNativeView = getRootView();
    if (drawerNativeView && drawerNativeView.showDrawer) {
        drawerNativeView.showDrawer();
    }
}

export const closeDrawer = () => {
    let drawerNativeView = getRootView();
    if (drawerNativeView && drawerNativeView.showDrawer) {
        drawerNativeView.closeDrawer();
    }
}

export const tf2t = (timeString) => { //take a xx:xx time string formatted for 24hr and return a 12hr formatted string
    const timeArray = timeString.split(":")
    const hour = parseInt(timeArray[0])
    const minute = parseInt(timeArray[1])
    const ampm = hour<12 ? "am" : "pm"
    return `${hour<13 ? hour : hour-12}:${minute<10 ? '0' + minute : minute}${ampm}`
}

export const date2String = (d) => {
    const year = d.getFullYear()
    const month = d.getMonth()
    const day = d.getDate()
    const output = `${year}` +
    `${month < 10 ? `0${month+1}` : `${month+1}`}` +
    `${day < 10 ? `0${day}` : `${day}`}`
    return output
}

export const dateString2Verbose = (d) => {
    const dayNames = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday']
    const monthNames = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']
    const year = Number(d.substring(0,4))
    const month = Number(d.substring(4,6))
    const dateNum = Number(d.substring(6,8))
    const dateObj = new Date(year, month-1, dateNum)
    const dateLetters = ()=>{
        if(dateNum === 1 || dateNum-20 === 1 || dateNum-30 === 1) return 'st'
        if(dateNum === 2 || dateNum-20 === 2) return 'nd'
        if(dateNum === 3 || dateNum-20 === 3) return 'rd'
        return 'th'
    }
    return `${dayNames[dateObj.getDay()]}, ${monthNames[month-1]} ${dateNum}${dateLetters()}, ${year}`
}

export function date2TimeString(d){
    const hours = d.getHours()
    const minutes = d.getMinutes()
    return `${hours}:${minutes < 10 ? `0${minutes}` : minutes}`
}